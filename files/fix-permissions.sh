#!/bin/bash

for name in $(sudo koha-list --enabled) ; do 
    kohaconfig="/etc/koha/sites/$name/koha-conf.xml"

    dir="$( xmlstarlet sel -t -v 'yazgfs/config/template_cache_dir' $kohaconfig )"

    (printf '%b' "$dir\0" && sudo find "$dir" -type d -print0) | sudo xargs -0 -r chmod g+rxws
    (printf '%b' "$dir\0" && sudo find "$dir" -type f -print0) | sudo xargs -0 -r chmod g+rws
    (printf '%b' "$dir\0" && sudo find "$dir" -type f -print0) | sudo xargs -0 -r setfacl -m g:www-data:rw
    (printf '%b' "$dir\0" && sudo find "$dir" -type d -print0) | sudo xargs -0 -r setfacl -m g:www-data:rwx
    (printf '%b' "$dir\0" && sudo find "$dir" -type d -print0) | sudo xargs -0 -r setfacl -d -m g:www-data:rwx
    (printf '%b' "$dir\0" && sudo find "$dir" -type f -print0) | sudo xargs -0 -r setfacl -m g:$name-koha:rw
    (printf '%b' "$dir\0" && sudo find "$dir" -type d -print0) | sudo xargs -0 -r setfacl -m g:$name-koha:rwx
    (printf '%b' "$dir\0" && sudo find "$dir" -type d -print0) | sudo xargs -0 -r setfacl -d -m g:$name-koha:rwx
done
