#!/bin/bash

for instance in $(koha-list); do
    chmod g+wxs /var/log/koha/$instance
    setfacl -m m::rwx /var/log/koha/$instance
    setfacl -d -m m::rwx /var/log/koha/$instance
    setfacl -d -m g:${instance}-koha:rw /var/log/koha/$instance
    setfacl -m g:${instance}-koha:rxw /var/log/koha/$instance
    setfacl -m g:${instance}-koha:rw /var/log/koha/$instance/*
    setfacl -d -m g:www-data:rw /var/log/koha/$instance
    setfacl -m g:www-data:rxw /var/log/koha/$instance
    setfacl -m g:www-data:rw /var/log/koha/$instance/*
    setfacl -m g:www-data:r /etc/koha/sites/$instance/log4perl.conf
done
