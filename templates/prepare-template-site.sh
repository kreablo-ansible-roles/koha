#!/bin/bash

sed -i 's/TMPL_ADMIN_CARD/admincard/g' template-site.sql.in
sed -i 's/TMPL_ADMIN_SURNAME/{{item.admin_surname}}/g' template-site.sql.in
sed -i 's/TMPL_ADMIN_FIRSTNAME/{{item.admin_firstname}}/g' template-site.sql.in
sed -i 's/TMPL_ADMIN_USERID/{{item.admin_userid}}/g' template-site.sql.in
sed -i 's/TMPLBRANCH_NAME/{{item.branchname}}/g' template-site.sql.in
sed -i 's/TMPLBRANCH/{{item.branchcode}}/g' template-site.sql.in
sed -i 's/TMPL_CAT_DESCRIPTION/{{item.borrowercategorydescr}}/g' template-site.sql.in
sed -i 's/TMPLCAT/{{item.borrowercategory}}/g' template-site.sql.in
